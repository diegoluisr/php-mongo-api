#!/usr/bin/env bash


# MONGODB INSTALL
# https://docs.mongodb.com/manual/tutorial/install-mongodb-on-ubuntu/

echo -e "\n--- Installing Process ---\n"

echo -e "\n--- Updating System ---\n"
apt-get -qq update

echo -e "\n--- Adding MongoDB 3.2 repo ---\n"
apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv EA312927 > /dev/null 2>&1
echo "deb http://repo.mongodb.org/apt/ubuntu trusty/mongodb-org/3.2 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.2.list

echo -e "\n--- Installing MongoDB ---\n"
apt-get install -y mongodb-org > /dev/null 2>&1

echo -e "\n--- Installing PHP and dependencies ---\n"
apt-get -y install php5 libapache2-mod-php5 php-pear php5-dev php5-curl > /dev/null 2>&1

echo -e "\n--- Enabling mod-rewrite ---\n"
a2enmod rewrite > /dev/null 2>&1

echo -e "\n--- Updating System (Again 1) ---\n"
apt-get -qq update

echo -e "\n--- Installing MongoDB libreries for PHP ---\n"
printf "\n" | pecl install mongo > /dev/null 2>&1

echo -e "\n--- Configuring MongoDB ---\n"
touch /etc/apache2/mods-available/mongo.ini
printf "; configuration for php mongo module\n; priority=20\nextension=mongo.so" > /etc/php5/mods-available/mongo.ini

ln -s /etc/php5/mods-available/mongo.ini /etc/php5/apache2/conf.d/20-mongo.ini

echo -e "\n--- Creating web server folders and symlinks ---\n"
if ! [ -d /vagrant/www ]; then
    mkdir /vagrant/www
fi

if ! [ -L /var/www ]; then
    rm -rf /var/www
    ln -fs /vagrant/www /var/www
fi

echo -e "\n--- We definitly need to see the PHP errors, turning them on ---\n"
sed -i "s/error_reporting = .*/error_reporting = E_ALL/" /etc/php5/apache2/php.ini
sed -i "s/display_errors = .*/display_errors = On/" /etc/php5/apache2/php.ini

echo -e "\n--- Change default HTML folder ---\n"
sed -i "s/DocumentRoot \/var\/www\/html/DocumentRoot \/var\/www/" /etc/apache2/sites-enabled/000-default.conf

echo -e "\n--- Allowing Apache override to all ---\n"
sed -i "s/AllowOverride None/AllowOverride All/g" /etc/apache2/apache2.conf

echo -e "\n--- Restaring Apache server ---\n"
apachectl restart > /dev/null 2>&1

echo -e "\n--- Installing composer ---\n"
curl --silent https://getcomposer.org/installer | php > /dev/null 2>&1
chmod +x composer.phar
mv composer.phar /usr/local/bin/composer

echo -e "\n--- END ---\n"