<?php

namespace Controllers;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

class UsersController extends AppController {

    protected $app = null;
    protected $result;


    /**
     * Contructor
     * @param Slim $app objeto aplicacion
     */

    function __construct(\Slim\App $app){
        $this->app = $app;
        $this->result = array();
    }
}