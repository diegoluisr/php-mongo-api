<?php

namespace Controllers;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

class LoginController extends AppController {

    /**
     * Contructor
     * @param Slim $app objeto aplicacion
     */

    function __construct(){
        parent::__construct();
    }

    public function add(Request $request, Response $response, $args) {
        $body = $request->getParsedBody();

        // $body = $this->app->request->getBody();
        // if($body != ''){
        //     $payload = json_decode($body);

        //     $email = property_exists($payload, 'Email') ? $payload->Email : null;
        //     $password = property_exists($payload, 'Password') ? $payload->Password : null;

        //     // $this->result['data'] = $payload;
        //     // print_r(Utils::secure($password));

        //     $user = UserQuery::create()
        //         ->filterByEmail($email)
        //         ->filterByPassword(Utils::secure($password))
        //         ->findOne();
        //     if(is_object($user)){
        //         $this->result['data'] = array('Token' => $this->createAccessToken($user));
        //     } else {
        //         $this->app->response->setStatus(400);
        //         $this->result['message'] = 'Email y/o contraseña no validos!';
        //     }
        // } else {
        //     $this->result['message'] = "No existen datos!";
        //     $this->app->response->setStatus(412);
        // }


        // return $response->withJson($this->result);
        return $response->withJson($body);
    }
}