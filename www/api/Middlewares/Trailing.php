<?php
namespace Middlewares;

use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
use \Propel\Runtime\ActiveQuery\Criteria;

/**
 * Middleware trail request path.
 */
class Trailing {
    const KEY = 'TRAILING';

    private $settings = [];

    public function __construct($settings = null) {
        if ($settings !== null) {
            $this->settings = array_merge($this->settings, $settings);
        }
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, callable $next) {
        $uri = $request->getUri();
        $path = $uri->getPath();
        if ($path != '/' && substr($path, -1) == '/') {
            $uri = $uri->withPath(substr($path, 0, -1));
            return $response->withRedirect((string)$uri, 301);
        }

        return $next($request, $response);
    }
}