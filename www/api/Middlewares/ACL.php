<?php
namespace Middlewares;

use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
use \Propel\Runtime\ActiveQuery\Criteria;

/**
 * Middleware returns the client ip.
 */
class ACL {

    const KEY = 'ACL';

    private $settings = [
        'name' => 'Token',
        'expire' => 'P1D',
    ];
    private $headers = [
        'Forwarded',
        'Forwarded-For',
        'Client-Ip',
        'X-Forwarded',
        'X-Forwarded-For',
        'X-Cluster-Client-Ip',
    ];


    /**
     * Constructor. Defines de trusted headers.
     *
     * @param null|array $headers
     */
    public function __construct($settings = null) {
        if ($settings !== null) {
            $this->settings = array_merge($this->settings, $settings);
        }
    }

    /**
     * Execute the middleware.
     *
     * @param ServerRequestInterface $request
     * @param ResponseInterface      $response
     * @param callable               $next
     *
     * @return ResponseInterface
     */
    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, callable $next) {
        $role = 'guest';

        $token = $request->getHeader($this->settings['name']);
        $ip = $this->getIp($request);
        $agent = $request->getHeader('User-Agent');

        $user = self::getUser($token, $ip, $agent);
        $roles = null;
        
        if(is_object($user)) {
            $roles = $user->getRoles();

            if(is_object($roles)) {
                $response = $response->withJson($roles->toArray());
            }
        }

        $module_name = 'core';
        $resource = '/';

        $params = explode('/', $request->getAttribute('params'));
        // $response = $response->withJson($params);

        if(count($params) > 0 && $params[0] != ''){
            $module = $params[0];
        }

        // PARA AGREGAR ATRIBUTOS ADICIONALES
        // $request = $request->withAttribute('foo', 'bar');

        return $next($request, $response);
    }

    private function getIp(ServerRequestInterface $request) {
        $server = $request->getServerParams();
        $ips = [];

        if (!empty($server['REMOTE_ADDR']) && filter_var($server['REMOTE_ADDR'], FILTER_VALIDATE_IP)) {
            $ips[] = $server['REMOTE_ADDR'];
        }
        foreach ($this->headers as $name) {
            $header = $request->getHeaderLine($name);
            if (!empty($header)) {
                foreach (array_map('trim', explode(',', $header)) as $ip) {
                    if ((array_search($ip, $ips) === false) && filter_var($ip, FILTER_VALIDATE_IP)) {
                        $ips[] = $ip;
                    }
                }
            }
        }

        return isset($ips[0]) ? $ips[0] : null;
    }

    public function getUser($token, $ip, $user_agent) {
        $access_token = \AccessTokenQuery::create()
            ->filterByToken($token)
            ->filterByIp($ip)
            ->filterByUserAgent($user_agent)
            ->filterByExpiresAt(array("min" => date('Y-m-d H:i:s')))
            ->findOne();

        if(is_object($access_token)){
            $time = new \DateTime(date('Y-m-d H:i:s'));
            $time->add(new \DateInterval($this->settings['expire']));
            $stamp = $time->format('Y-m-d H:i:s');
            $access_token->setExpiresAt($stamp);
            $access_token->save();


            return $access_token->getUser();
        }
        return null;
    }
}