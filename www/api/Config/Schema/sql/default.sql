
# This is a fix for InnoDB in MySQL >= 4.1.x
# It "suspends judgement" for fkey relationships until are tables are set.
SET FOREIGN_KEY_CHECKS = 0;

-- ---------------------------------------------------------------------
-- entities
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `entities`;

CREATE TABLE `entities`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(45) NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- roles
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `roles`;

CREATE TABLE `roles`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(10) NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- users
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `external_id` VARCHAR(30) NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- users_roles
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `users_roles`;

CREATE TABLE `users_roles`
(
    `user_id` INTEGER NOT NULL,
    `role_id` INTEGER NOT NULL,
    PRIMARY KEY (`user_id`,`role_id`),
    INDEX `fi__rol_fk` (`role_id`),
    CONSTRAINT `rol_usr_fk`
        FOREIGN KEY (`user_id`)
        REFERENCES `users` (`id`),
    CONSTRAINT `usr_rol_fk`
        FOREIGN KEY (`role_id`)
        REFERENCES `roles` (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- file_types
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `file_types`;

CREATE TABLE `file_types`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `entity_id` INTEGER NOT NULL,
    `name` VARCHAR(45) NOT NULL,
    `mimetypes_allowed` VARCHAR(255) NOT NULL,
    `max_allowed` TINYINT NOT NULL,
    PRIMARY KEY (`id`),
    INDEX `fi_typ_ent_fk` (`entity_id`),
    CONSTRAINT `rsctyp_ent_fk`
        FOREIGN KEY (`entity_id`)
        REFERENCES `entities` (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- files
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `files`;

CREATE TABLE `files`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `file_type_id` INTEGER NOT NULL,
    `related_id` INTEGER NOT NULL,
    `uploaded_at` DATETIME,
    `deleted_at` DATETIME,
    `filename` VARCHAR(255) NOT NULL,
    `metadata` VARCHAR(255),
    PRIMARY KEY (`id`),
    INDEX `rsc__rlt_fk` (`related_id`),
    INDEX `fi__rsctyp_fk` (`file_type_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- access_tokens
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `access_tokens`;

CREATE TABLE `access_tokens`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `user_id` INTEGER NOT NULL,
    `expires_at` DATETIME,
    `ip` VARCHAR(45) NOT NULL,
    `token` VARCHAR(45) NOT NULL,
    `user_agent` VARCHAR(255) NOT NULL,
    PRIMARY KEY (`id`),
    INDEX `fi__usr_fk` (`user_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- resources
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `resources`;

CREATE TABLE `resources`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `path` VARCHAR(45) NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- permissions
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `permissions`;

CREATE TABLE `permissions`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `role_id` INTEGER NOT NULL,
    `resource_id` INTEGER NOT NULL,
    `method` VARCHAR(10) NOT NULL,
    PRIMARY KEY (`id`),
    INDEX `fi__rol_fk` (`role_id`),
    INDEX `fi__res_fk` (`resource_id`)
) ENGINE=MyISAM;

# This restores the fkey checks, after having unset them earlier
SET FOREIGN_KEY_CHECKS = 1;
