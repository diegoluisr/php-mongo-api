<?php
// http://docs.slimframework.com/
/* ----------------------------------------------------------------------------------------------------------------------------
__________  ___ _____________             _____   ________    _______    ________ ________               _____ __________.___ 
\______   \/   |   \______   \           /     \  \_____  \   \      \  /  _____/ \_____  \             /  _  \\______   \   |
 |     ___/    ~    \     ___/  ______  /  \ /  \  /   |   \  /   |   \/   \  ___  /   |   \   ______  /  /_\  \|     ___/   |
 |    |   \    Y    /    |     /_____/ /    Y    \/    |    \/    |    \    \_\  \/    |    \ /_____/ /    |    \    |   |   |
 |____|    \___|_  /|____|             \____|__  /\_______  /\____|__  /\______  /\_______  /         \____|__  /____|   |___|
                 \/                            \/         \/         \/        \/         \/                  \/              
---------------------------------------------------------------------------------------------------------------------------- */

require 'vendor/autoload.php';
require 'Config/config.php';
$settings = require 'Config/settings.php';

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Utils\MongoWrapper as Mongo;

// use Controllers\*;

$app = new \Slim\App($settings);

// https://github.com/oscarotero/psr7-middlewares
// http://help.slimframework.com/discussions/questions/8833-slim-3-pass-variables-from-middleware

// SUPER LIBRERIA
// https://github.com/ziadoz/awesome-php#micro-frameworks
$app->add(new Middlewares\Trailing());
$app->add(new Middlewares\Payload());
// $app->add(new Middlewares\ACL());

$app->map(['GET', 'POST', 'PUT', 'DELETE'], '/[{params:.*}]', function ($request, $response, $args) use ($app) {
    $params = explode('/', $request->getAttribute('params'));

    if(count($params) == 1 && $params[0] == '') {
        $response = $response->withJson(array('api' => 'php-mongo-api'));
    } else {
        if(count($params) >= 1) {
            $ctlr_name = \Utils\Utils::Slug2CamelCase($params[0]);

            $class = new ReflectionClass('\\Controllers\\'.$ctlr_name . 'Controller');
            $instance = $class->newInstanceArgs(array($app));

            switch ($request->getMethod()) {
                case 'GET':
                    return $instance->index($request, $response, $args);
                    break;
                case 'POST':
                    return $instance->add($request, $response, $args);
                    break;
                case 'PUT':
                    return $instance->edit($request, $response, $args);
                    break;
                case 'DELETE':
                    return $instance->delete($request, $response, $args);
                    break;
                default:
                    return $response->withJson(array('message' => 'Unavailable method'));
                    break;
            }

            // try{
                
            //     switch ($request->getMethod()) {
            //         case 'GET':
            //             $cursor = $collection->find();
            //             $data = [];
            //             foreach ($cursor as $document) {
            //                 $data[] = array("id"=> (string) $document['_id'], "code"=>$document['code']);
            //                 // var_dump($document);
            //             }
            //             $response = $response->withJson($data);
            //             break;
            //         case 'POST':
            //             $vehicle = array( "code" => "DLR123", "type" => "Car" );
            //             $collection->insert($vehicle);
            //             break;             
            //         default:
            //             $response = $response->withJson(array('message' => 'Unavailable method'));
            //             break;
            //     }
            // } catch (\Exception $e) {
            //     $response = $response->withJson(array('error'=>'ReflectionException','message' => $e->getMessage()));
            // }
        } else {
            $response = $response->withJson(array('message' => 'Unavailable number of params'));
        }
    }

    return $response;
});

$app->run();