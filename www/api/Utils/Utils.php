<?php

namespace Utils;

/**
 * /ext/Utils.php
 * 
 * Clase con funciones estaticas de apoyo
 * @author Diego Luis Restrepo <diegoluisr@gmail.com>
 * @version 1.0
 * @package None
 */
class Utils {

    /**
     * Funcion que se le entrega una cadena en "slug_format" y lo devuelve en formato "CamelCase"
     * @param string $slug
     * @return string 
     */
    public static function Slug2CamelCase($slug) {
        $slug = str_replace(array('-', '_'), ' ', $slug);
        $slug = ucwords($slug);
        $slug = str_replace(' ', '', $slug);
        return $slug;
    }

    /**
     * Funcion que se entrega un token unico
     * @return string 
     */
    public static function createToken() {
        return bin2hex(openssl_random_pseudo_bytes(16));
    }

    /**
     * Funcion que se cifra una cadena en formato SHA256
     * @param string $str
     * @return string 
     */
    public static function secure($str) {
        return hash('sha256', $str);
    }

    /**
     * Funcion que devuelve las palabras mayores a 3 caracteres de una frase
     * @param string $str
     * @return array 
     */
    public static function getWords($str) {
        $new_words = array();
        if(isset($str) && !empty($str)) {
            $words = array_unique(explode(' ', $str));
            foreach ($words as $word) {
                if(strlen($word) >= 3){
                    $new_words[] = $word;
                }
            }
        }
        if(count($new_words) == 0) {
            return null;
        }
        return $new_words;
    }
}