from flask import Flask
from flask import request

app = Flask(__name__)

@app.route("/<string:collection>")
def handler(collection):
    # if request.method == 'POST':
    return request.method

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=80)